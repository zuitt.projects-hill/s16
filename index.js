//alert("Holla atcha Boy!")

function displayMsgToSelf(){
	console.log("Greetings from the future, don't drink the coffee today, it is poisoned!")
};

/*displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();*/

let count = 10;

while(count !== 0){
	displayMsgToSelf();
	count--;
};

//While Loop
	//will allow us to repeat an instruction as long as the condition is true.
	/*
		Syntax:
		while (condition is true){
			statement/instruction runs
			incrementation or decrementation to
			stop the loop
		};
	*/

let number = 5;
//countdown from 5-1
while (number !== 0){
	console.log("While" + " " + number);
	number--;
};

//count from 5-10
while (number <= 10){
	console.log(`While ${number}`);
	number++;
};

//Do-While Loop
	
	let number2 = Number(prompt("Give me a number: "))

	do{
		console.log(`Do While ${number2}`);

		number2 += 1;
	} while(number2 < 10);

	let counter = 1;

	do{
		console.log(counter);
		counter++;
	} while (counter <= 21);

//For Loop

for(let count = 0; count <= 20; count++){
	console.log(count);
};

//Syntax:
/*
	for(initialization; condition; finalExpression){
		statement;
	};
*/

let myString = "alex";
console.log(myString.length);

console.log(myString[0]);
	//result: a

for(let x = 0; x < myString.length; x++){

	console.log(myString[x]);
};

let myName = "Thomas";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		){
			console.log(3);

	} else {
		console.log(myName[i]);
	}
};

	for(let count = 0; count <= 20; count++){

		if (count % 2 === 0) {
			continue;
		}
		console.log(`Continue and Break ${count}`)

		if (count > 10) {
			break;
		}
	};


	let name = "alexandro";

	for (let i = 0; i < name.length; i++){
		console.log(name[i]);

		if(name[i].toLowerCase() === "a"){
			console.log("Continue to the next iteration")
			continue;

		}
		if(name[i] == "d"){
			break;
		}
	};
	